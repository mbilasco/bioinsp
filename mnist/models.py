class NeuronMvgThsldVMaxInhibitionRefract_M(object):
	model=''' '''
	reset = ''' '''
	thshld = ''' '''
	def __init__(self,presentation_window):
		self.thshld = '''v>movingThresh and v==vMax'''
		self.rst='''v = vr
			increment_thresh = ((1==1) or (((lastSpike>0*ms) and ((t-lastSpike)<time_thresh))==True))+0
			raw_thresh = movingThresh+increment_thresh*thresh_incr
			movingThresh = clip(raw_thresh,thresh,4*thresh_incr+thresh) 
			lastSpike=t'''
		self.model='''time_thresh = %f*ms : second
	        tLeak = %f * ms: second
	        threshLeak = %f * ms : second
	        tRefrac = %f*ms : second
	        tRefracLeak = %f*ms : second
	        
	        thresh = -50*mV: volt
	        vr = -72*mV:volt
	        vl = -60*mV:volt
	        lastSpike:second
	        
	        dv/dt = (((t-lastSpike)>tRefrac or (lastSpike==0*ms))==True)*(vl - v)/tLeak +
	                (False==((t-lastSpike)>tRefrac or (lastSpike==0*ms)))*(vl-v)/tRefracLeak : volt 
	        dmovingThresh/dt = (thresh - movingThresh)/threshLeak : volt
	        
	        thresh_incr = 0*mV : volt
	        
	        vMax = nanmax(v/volt) * volt: volt
	        apprentissage : 1
	        inhibition : 1
	        increment_thresh : 1
	        raw_thresh : volt
	        lastInhibition:second
	        threshInhibition = %f * ms : second
	        ''' % (presentation_window/4, #time_thresh
	               presentation_window/4, #tLeak
	               presentation_window/4, #treshLeak
	               presentation_window/2, #tRefrac
	               (presentation_window/8),    #tRefracLeak
	               presentation_window/4) #threshInhibition
	        
class SynapsesSTDPRefract_M(object):
	
	model=''' '''
	preEqs=''' '''
	postEqs=''' '''
	
	def __init__(self,wMin,wMax,tSTDP,aPre,aPost):
		self.preEqs = '''tPre = t
			condV=((tPre - lastInhibition_post >= threshInhibition_post) or \
				(lastInhibition_post == 0 * ms)) and ((tPre-lastSpike_post>tRefrac_post) or (lastSpike_post==0*ms) )
			v_post +=  w * (condV)
			aPost += %f*mV
			wn=(clip(w + aPre , wMin, wMax) * apprentissage_post)
			w = w * (1 - apprentissage_post) + wn''' % (aPost)
	
		self.model='''
			daPre/dt = ( 0 - aPre)/ (%d * ms) : volt (event-driven)
			daPost/dt = (0 -  aPost)/ (%d * ms) : volt (event-driven)
			wMax = %f*mV : volt
			wMin = %f*mV : volt
			w : volt
			wp:volt
			wn:volt
			tPre:second
			tPost:second
		''' % (tSTDP,tSTDP,wMax,wMin);
		
		self.postEqs = '''
			aPre -=  %f*mV
			wp= (clip(w + aPost, wMin, wMax) * apprentissage_post)
			w = w * (1 - apprentissage_post) + wp
		''' % (aPre)
        
		
class SynapsesInhib_M(object):
	model=''
	pre=''
	def __init__(self):
		self.model='''w:volt'''
		self.pre='''lastInhibition_post=t; v_post=vl_post'''