from mnist import MNIST
from io import open
import conf

#/data/sandbox/mnist/
#/datasets/MNIST/

mndata = MNIST(conf.DATASET_FOLDER)
mndata.load_training()
mndata.load_testing()

nb = len(mndata.train_images)

text_file = open(conf.DATASET_FOLDER+"/mnist_train_images.csv", "w")

for i in range(0, nb) :
#for i in range(0, 20) :
	if i % 100 == 0 :
		print i
	text_file.write(u"%d," % mndata.train_labels[i])
	for x in range (0, len(mndata.train_images[i]) ):
		text_file.write(u" %d" % mndata.train_images[i][x])
	text_file.write(u"\n")
text_file.close();

text_file = open(conf.DATASET_FOLDER+"/mnist_test_images.csv", "w")
	
nb = len(mndata.test_images)
for i in range(0, nb) :
#for i in range(0, 20) :
	if i % 100 == 0 :
		print i
	text_file.write(u"%d," % mndata.test_labels[i])
	for x in range (0, len(mndata.test_images[i]) ):
		text_file.write(u" %d" % mndata.test_images[i][x])
	text_file.write(u"\n")

text_file.close();
