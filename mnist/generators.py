import numpy;
import conf;
import csv;
import brian2;

def generate_input_spikes(imgs_array,method,presentation_time,factor,MAX_SPIKES=0):
    offset = 0
    spike_times_ids = []; spike_times_time=[]
    for i in range (0, len(imgs_array)) :
        spike_times_i_idx,spike_times_i_time=method(imgs_array[i],presentation_time,factor,MAX_SPIKES)
        
        if conf.LOGS_SPIKE_GENERATOR :
            filename=conf.OUTPUT_FOLDER+'/'+str(method.__name__)+"_csv/"+str(i)+".csv"
            print "saving spike train to "+filename
            with open(filename,'w') as f:
                w=csv.writer(f)
                w.writerow(spike_times_i_idx);
                w.writerow(spike_times_i_time);
                f.close()
            
        for j in range(0,len(spike_times_i_time)):
            spike_times_i_time[j]=(spike_times_i_time[j]+offset)
            
        spike_times_ids.extend(spike_times_i_idx)
        spike_times_time.extend(spike_times_i_time)
        
        offset=offset+presentation_time
    
    if conf.LOGS_SPIKE_GENERATOR :
        filename=conf.OUTPUT_FOLDER+'/'+str(method.__name__)+"_csv/"+"all"+".csv"
        with open(filename,'w') as f:
            w=csv.writer(f)
            w.writerow(spike_times_ids)
            w.writerow(spike_times_time)
            f.close()
    return (spike_times_ids,spike_times_time)
    
def intensity_spikes_no_window(img_array,presentation_time,factor,MAX_SPIKES):
    nb_neurons = len(img_array)
    spike_trains_idx=[];spike_trains_time=[];
    nb_spikes = 0
    for i in range (0,nb_neurons-1):
        time_all=0
        
        if (img_array[i]==0):
            #fire_rate=3*presentation_time
            continue
        else: 
            fire_rate=(presentation_time/(factor*img_array[i]/255.))
        
        while (time_all<presentation_time/2) :                            
            time=numpy.random.poisson(fire_rate)
            while time<1e-9 :
                time=numpy.random.poisson(fire_rate)
                print 'regenerating spike for neuron %d'%i
            time_all=time+time_all
            if (time_all<presentation_time/2):
                spike_trains_idx.append(i);
                spike_trains_time.append(time_all);
                nb_spikes = nb_spikes + 1

    
    for j in range (0,nb_spikes-1) :
        for k in range (j+1,nb_spikes) :
            if (spike_trains_time[j]>spike_trains_time[k]) :
                x=spike_trains_time[j]; spike_trains_time[j]=spike_trains_time[k]; spike_trains_time[k]=x;
                x=spike_trains_idx[j]; spike_trains_idx[j]=spike_trains_idx[k]; spike_trains_idx[k]=x

            
    return (spike_trains_idx,spike_trains_time)

def intensity_spikes_no_window_with_noise(img_array,presentation_time,factor,MAX_SPIKES):
    nb_neurons = len(img_array)
    spike_trains_idx=[];spike_trains_time=[];
    nb_spikes = 0
    for i in range (0,nb_neurons-1):
        time_all=0
        
        if (img_array[i]==0):
            fire_rate=3*presentation_time
        else: 
            fire_rate=(presentation_time/(factor*img_array[i]/255.))
        
        while (time_all<presentation_time/2) :                            
            time=numpy.random.poisson(fire_rate)
            while time<1e-9 :
                time=numpy.random.poisson(fire_rate)
                print 'regenerating spike for neuron %d'%i
            time_all=time+time_all
            if (time_all<presentation_time/2):
                spike_trains_idx.append(i);
                spike_trains_time.append(time_all);
                nb_spikes = nb_spikes + 1
    
    #add noise
    for i in range (0,nb_neurons-1):
        time_all=presentation_time/2
        
        if (img_array[i]>0):
            fire_rate=6*presentation_time
        else: 
            fire_rate=(presentation_time/((255-img_array[i])/255.)/2)
        
        while (time_all<presentation_time) :                            
            time=numpy.random.poisson(fire_rate)
            while time<1e-9 :
                time=numpy.random.poisson(fire_rate)
                print 'regenerating spike for neuron %d'%i
            time_all=time+time_all
            if (time_all<presentation_time):
                spike_trains_idx.append(i);
                spike_trains_time.append(time_all);
                nb_spikes = nb_spikes + 1
    
    for j in range (0,nb_spikes-1) :
        for k in range (j+1,nb_spikes) :
            if (spike_trains_time[j]>spike_trains_time[k]) :
                x=spike_trains_time[j]; spike_trains_time[j]=spike_trains_time[k]; spike_trains_time[k]=x;
                x=spike_trains_idx[j]; spike_trains_idx[j]=spike_trains_idx[k]; spike_trains_idx[k]=x

            
    return (spike_trains_idx,spike_trains_time)

def intensity_N_spikes_no_window(img_array,presentation_time,factor,MAX_SPIKES=0):
    nb_neurons = len(img_array)
    spike_trains_idx=[];spike_trains_time=[];
    nb_spikes = 0
    for i in range (0,nb_neurons-1):
        time_all=0
        
        if (img_array[i]==0):
            fire_rate=3*presentation_time
        else: 
            fire_rate=(presentation_time/(factor*img_array[i]/255.))
        
        while (time_all<presentation_time/2) :                            
            time=numpy.random.poisson(fire_rate)
            while time<1e-9 :
                time=numpy.random.poisson(fire_rate)
                print 'regenerating spike for neuron %d'%i
            time_all=time+time_all
            if (time_all<presentation_time/2):
                spike_trains_idx.append(i);
                spike_trains_time.append(time_all);
                nb_spikes = nb_spikes + 1
            
    for j in range (0,nb_spikes-1) :
        for k in range (j+1,nb_spikes) :
            if (spike_trains_time[j]>spike_trains_time[k]) :
                x=spike_trains_time[j]; spike_trains_time[j]=spike_trains_time[k]; spike_trains_time[k]=x;
                x=spike_trains_idx[j]; spike_trains_idx[j]=spike_trains_idx[k]; spike_trains_idx[k]=x

    if (MAX_SPIKES==0) :
        return (spike_trains_idx,spike_trains_time)
    else :
        _max=min(MAX_SPIKES,nb_spikes)
        return (spike_trains_idx[1:_max],spike_trains_time[1:_max])