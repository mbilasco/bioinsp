from mnist import MNIST
from io import open
from architectures import *
from visu import plot_spikes_matrix
import numpy
import filters
import generators
import brian2
import conf

#/data/sandbox/mnist/
#/datasets/MNIST/

mndata = MNIST('/datasets/MNIST/')
mndata.load_training()
mndata.load_testing()

#net=one_layer_archi(10)
presentation_time=200
train_images=filters.filter_images_by_label(mndata.train_images,mndata.train_labels,[0,1]);

conf.LOGS_SPIKE_GENERATOR=True         
list_spikes=generators.generate_input_spikes(train_images,
                                 generators.intensity_spikes_no_window,
                                 presentation_time,
                                 10)

plot_spikes_matrix(list_spikes,presentation_time,presentation_time/5)
