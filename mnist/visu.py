import numpy
import pylab
import matplotlib.pyplot as plt
import conf
import brian2

def save_mat_to_gray_img(mat,img_file,vmin=None,vmax=None):
    plt.matshow(mat, fignum=100, cmap=plt.gray(),vmin=vmin, vmax=vmax)
    pylab.savefig(img_file);
    #print 'writing '+conf.OUTPUT_FOLDER+'/input_spikes/'+str(iteration)+'_'+str(inner_iteration)+'.png'
    plt.close()
    
def save_synapses_as_img(synapses,list_out_neurons,name_suffix,size=[28,28],vmin=None,vmax=None):
    mats=[]
    for i in range(0,len(list_out_neurons)):
        mats.append(numpy.zeros(size))
    
    mix=zip(synapses.i,synapses.j,synapses.w / brian2.volt)  
    #print mix  
    for _,(i,j,w) in enumerate(mix) :
        #print i, " - ", j, " - ", w
        mats[j][i/size[1],i%size[1]]=w
    
    for i in range(0,len(list_out_neurons)):
        mats[i]/=brian2.volt;
        filename=conf.OUTPUT_FOLDER+'/synapses/'+str(i)+'_'+name_suffix+'.png'
        save_mat_to_gray_img(mats[i],filename,vmin=vmin,vmax=vmax)

def save_spikes_as_mat(spikeMonitor,nb_cols,layer_name):
    #print spikeMonitor.name
    #print spikeMonitor.t/brian2.ms
    #print spikeMonitor.i
    plt.plot(spikeMonitor.t,spikeMonitor.i,'.k')
    pylab.savefig(conf.OUTPUT_FOLDER+"/spikes/"+layer_name+'.png');
    plt.close()
    
def save_spikes_as_mat_btwn_times(spikeMonitor,nb_cols,layer_name, start,end):
    #print spikeMonitor.name
    #print spikeMonitor.t/brian2.ms
    #print spikeMonitor.i
    i=0
    while(i<len(spikeMonitor.t) and (start>spikeMonitor.t[i])) :
        i+=1
    st_idx=i
    while (i<len(spikeMonitor.t) and (end>spikeMonitor.t[i])):
        i+=1
    end_idx=i
    
    plt.plot(spikeMonitor.t[st_idx:end_idx]/brian2.ms,spikeMonitor.i[st_idx:end_idx],'.k')
    pylab.savefig(conf.OUTPUT_FOLDER+"/spikes/"+layer_name+'.png');
    plt.close()
    
def display_static_statemonitor_as_string(statemonitor,subset,layer_name,start=0,end=0):
    start=start if (start>0) else 0
    print layer_name," - ",subset, " - ", statemonitor
    for i in range(0,len(subset)):
        if (subset[i]>=len(statemonitor)):
            continue
        print layer_name," - ",i, "-", subset[i], " - ", statemonitor[subset[i]]

def save_statemonitor_as_multiplot(statemonitor,subset,layer_name,start=0,end=0):
    start=start if (start>0) else 0
    #print (len(statemonitor))
    
    for i in range(0,len(subset)):
        #print layer_name," - ",i, "-", subset[i], " - ", statemonitor
        if (subset[i]>=len(statemonitor)):
            continue
        #print layer_name," - ",i, "-", subset[i], " - ", statemonitor, " - ", statemonitor[subset[i]]
        x=statemonitor[subset[i]]
        l_end=end if end>0 else x.__len__()
        if (l_end <= start) :
            continue
        #print statemonitor[subset[i]][start:l_end]
        #print "ploting ", range(start,l_end), " for subset ", subset[i], " with ",len(statemonitor[subset[i]])
        plt.plot(range(start,l_end),x[start:l_end])
    pylab.savefig(conf.OUTPUT_FOLDER+"/states/"+str(subset)+'_'+layer_name+'.png');
    plt.close()
        
def save_statemonitor_as_plot(statemonitor,subset,layer_name,start=0,end=0):
    start=start if (start>0) else 0
    #print (len(statemonitor))
    
    for i in range(0,len(subset)):
        #print layer_name," - ",i, "-", subset[i], " - ", statemonitor
        if (subset[i]>=len(statemonitor)):
            continue
        #print layer_name," - ",i, "-", subset[i], " - ", statemonitor, " - ", statemonitor[subset[i]]
        x=statemonitor[subset[i]]
        l_end=end if end>0 else x.__len__()
        if (l_end <= start) :
            continue
        #print statemonitor[subset[i]][start:l_end]
        #print "ploting ", range(start,l_end), " for subset ", subset[i], " with ",len(statemonitor[subset[i]])
        plt.plot(range(start,l_end),x[start:l_end])
        pylab.savefig(conf.OUTPUT_FOLDER+"/states/"+str(subset[i])+'_'+layer_name+'.png');
        plt.close()
    
def plot_spikes_matrix(list_spikes_ids,list_spikes_times,presentation_time,intermediate_time,nb_images=0):
    i = 1 
    nb_spikes=len(list_spikes_ids)
    
    limit = 0
    offset = 0
    iteration = 0
    while ((i<nb_spikes) and (nb_images==0 or iteration<nb_images)):
        mat=numpy.zeros([28,28])
        offset = limit ; limit += presentation_time; 
        #print "x", iteration, i, list_spikes[i]
        (indice,temps)=(list_spikes_ids[i],list_spikes_times[i])
        inner_limit = offset+intermediate_time
        inner_iteration=0;j=0;
        while (temps<=limit and i<nb_spikes) :
            while (temps<=inner_limit and i<nb_spikes) : 
                mat[indice/28,indice%28]+=1
                i+=1;j+=1;
                if(i<nb_spikes) :
                    (indice,temps)=(list_spikes_ids[i],list_spikes_times[i])
                    #print "     ",i, indice,offset,limit
            img_file=conf.OUTPUT_FOLDER+'/input_spikes/'+str(iteration)+'_'+str(inner_iteration)+'_'+str(j)+'.png'
            save_mat_to_gray_img(mat,img_file)
            inner_iteration+=1;inner_limit+=intermediate_time;
        iteration+=1