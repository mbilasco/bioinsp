from mnist import MNIST
from io import open
from architectures import *
from visu import plot_spikes_matrix
import numpy
import filters
import generators
import brian2
import conf
import architectures;
import visu;
import genGraph;

#/data/sandbox/mnist/
#/datasets/MNIST/

mndata = MNIST(conf.DATASET_FOLDER)
mndata.load_training()
mndata.load_testing()

#net=one_layer_archi(10)
presentation_time=15
#list_labels,train_images=filters.filter_images_by_label(mndata.train_images,mndata.train_labels,[0,1],max_samples=20);
#list_labels0,test_images0=filters.filter_images_by_label(mndata.test_images,mndata.test_labels,[0],max_samples=10);
#list_labels1,test_images1=filters.filter_images_by_label(mndata.test_images,mndata.test_labels,[1],max_samples=10);

#all_images=train_images+test_images0+test_images1
#all_labels=list_labels+list_labels0+list_labels1

list_labelst0,train_images0=filters.filter_images_by_label(mndata.train_images,mndata.train_labels,[0],max_samples=100);
list_labelst1,train_images1=filters.filter_images_by_label(mndata.train_images,mndata.train_labels,[1],max_samples=100);
list_labels0,test_images0=filters.filter_images_by_label(mndata.test_images,mndata.test_labels,[0],max_samples=10);
list_labels1,test_images1=filters.filter_images_by_label(mndata.test_images,mndata.test_labels,[1],max_samples=10);

train_images=train_images0+train_images1

all_images=train_images0+train_images1+test_images0+test_images1
all_labels=list_labelst0+list_labelst1+list_labels0+list_labels1

print len(all_images), '-', all_labels

conf.LOGS_SPIKE_GENERATOR=False   
list_spikes_ids,list_spikes_time=generators.generate_input_spikes(
                                             all_images,
                                             #generators.intensity_N_spikes_no_window,
                                             generators.intensity_spikes_no_window_with_noise,
                                             presentation_time,
                                             factor=15,
                                             MAX_SPIKES=0)

#plot_spikes_matrix(list_spikes_ids,list_spikes_time,presentation_time,presentation_time/4,10)

net=architectures.one_layer_archi([array(list_spikes_ids), array(list_spikes_time)],2,presentation_time)

outState=StateMonitor(net.__getitem__('outLayer'),('v','movingThresh','lastSpike','lastInhibition','increment_thresh'),  record = True,name='outState') # Record the state of each neurons of the layer layer
synState = StateMonitor(net.__getitem__('synapses'), ('v','tPre','tPost','w'), record = True, name='synState') 
spikeOutLayer = SpikeMonitor(net.__getitem__('outLayer'), name='spikeOutLayer')
spikeInLayer = SpikeMonitor(net.__getitem__('inLayer'), name='spikeInLayer')
net.add(outState)
net.add(synState)
net.add(spikeInLayer)
net.add(spikeOutLayer)

pas=5
for i in range(1,pas+1):
    timeRun = (len(train_images)/pas)*presentation_time*brian2.ms
    net.run(timeRun, report='stdout')
    visu.save_spikes_as_mat((net.__getitem__('spikeInLayer')),28,"mnist_input_"+str(i))
    visu.save_spikes_as_mat_btwn_times((net.__getitem__('spikeInLayer')),28,"mnist_single_input_"+str(i),timeRun*(i-1),timeRun*(i))
    visu.save_spikes_as_mat(net.__getitem__('spikeOutLayer'),2,"mnist_output_"+str(i))
    visu.save_synapses_as_img(net.__getitem__('synapses'),[0,1],'mnist_inter_'+str(i))
    visu.save_statemonitor_as_multiplot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(i))
    visu.save_statemonitor_as_plot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(i))
    visu.save_statemonitor_as_plot(net.__getitem__('outState').lastInhibition, [0,1], 'mnist_Inhib_'+str(i))
    visu.save_statemonitor_as_plot(net.__getitem__('outState').movingThresh, [0,1], 'mnist_movingThresh_'+str(i))
    

net.__getitem__('outLayer').apprentissage=0
net.__getitem__('outLayer').inhibition=0

timeRun = len(test_images0)*presentation_time*brian2.ms
net.run(timeRun, report='stdout')
visu.save_spikes_as_mat(net.__getitem__('spikeInLayer'),28,"mnist_input_"+str(pas+1))
visu.save_spikes_as_mat(net.__getitem__('spikeOutLayer'),2,"mnist_output_"+str(pas+1))
visu.save_synapses_as_img(net.__getitem__('synapses'),[0,1],'mnist_inter_'+str(pas+1))
visu.save_statemonitor_as_multiplot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(pas+1))
visu.save_statemonitor_as_plot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(pas+1))
visu.save_statemonitor_as_plot(net.__getitem__('outState').movingThresh, [0,1], 'mnist_movingThresh_'+str(pas+2))
visu.save_statemonitor_as_plot(net.__getitem__('outState').lastInhibition, [0,1], 'mnist_Inhib_'+str(pas+1))

timeRun = len(test_images1)*presentation_time*brian2.ms
net.run(timeRun, report='stdout')
visu.save_spikes_as_mat(net.__getitem__('spikeInLayer'),28,"mnist_input_"+str(pas+2))
visu.save_spikes_as_mat(net.__getitem__('spikeOutLayer'),2,"mnist_output_"+str(pas+2))
visu.save_synapses_as_img(net.__getitem__('synapses'),[0,1],'mnist_inter_'+str(pas+2))
visu.save_statemonitor_as_multiplot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(pas+2))
visu.save_statemonitor_as_plot(net.__getitem__('outState').v, [0,1], 'mnist_vout_'+str(pas+2))
visu.save_statemonitor_as_plot(net.__getitem__('outState').movingThresh, [0,1], 'mnist_movingThresh_'+str(pas+2))
visu.save_statemonitor_as_plot(net.__getitem__('outState').lastInhibition, [0,1], 'mnist_Inhib_'+str(pas+2))

