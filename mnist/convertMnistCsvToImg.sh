count=0;
for idx in train test
do
	rm -rf ../../${idx}
	rm -rf ../../${idx}*.txt
	mkdir ../../$idx
	echo -n processing $idx ...
	cat ../../*${idx}*.csv | while read line 
	do
		echo $line | cut -f 2 -d ',' > /tmp/line.csv
		numero=`echo $line | cut -f 1 -d ','` 
		octave <<XXX
I=imread("/tmp/48x48.png");
data=dlmread("/tmp/line.csv"," ");
I=rand(28,28);
for i = 1:28 
  for j= 1:28
    I(i,j)=data((i-1)*28+j)/255.;
  endfor
endfor
imwrite(I,"/tmp/output.png");
XXX
		long_count=`echo 00000${count} | sed 's/.*\([0-9][0-9][0-9][0-9][0-9]\)/\1/'`
		mv /tmp/output.png ../../${idx}/${idx}_${long_count}.png
		echo ${idx}/${idx}_${long_count}.png >> ../../${idx}_${numero}.txt
		echo $numero ";" ${idx}/${idx}_${long_count}.png >> ../../${idx}.txt
		echo -n $count ...
		count=$(( $count + 1 ))
	done
	echo done
done
