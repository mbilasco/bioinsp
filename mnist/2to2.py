from brian2 import *
import random
from numpy import *

import visu

nb_in_neurons=2
nb_out_neurons=2

in_neurons=SpikeGeneratorGroup(2,indices=array([0,0,0,1,1,1]),times=(array([5,10,15,15,20,25]))*ms)
#(((lastSpike!=0*ms) and (t-lastSpike>tRefrac))==True
eqs = '''
        dv/dt = (((t-lastSpike)>tRefrac or (lastSpike==0*ms))==True)*(vl - v)/tLeak +
                (False==((t-lastSpike)>tRefrac or (lastSpike==0*ms)))*(vl-v)/tRefracLeak : volt 
        g:volt
        dmovingThresh/dt = (thresh - movingThresh)/threshLeak : volt
        lastInhib : second
        thresh = -54*mV: volt
        thresh_incr = 5*mV : volt
        thresh_t = 3*ms : second
        vr = -72*mV:volt
        vl = -60*mV:volt
        tLeak = 5 * ms: second
        threshLeak = 4*tRefrac: second
        vMax = nanmax(v/volt) * volt: volt
        tRefrac = 4*ms : second
        tRefracLeak = sqrt(tRefrac/ms)/2*ms : second
        tInhib : second
        apprentissage : 1
        reset : 1
        lastSuccessfulThresh : volt
        lastSpike:second
        increment_thresh : 1
        ''' 
#g=-vl+vr 
rst = '''v = vr
         reset=reset + 1
         lastSuccessfulThresh = movingThresh
         increment_thresh = (((lastSpike>0*ms) and ((t-lastSpike)<thresh_t))==True)+0
         raw_thresh = movingThresh+increment_thresh*thresh_incr
         movingThresh = clip(raw_thresh,thresh,4*thresh_incr+thresh)
         
         lastSpike=t
    '''
    #movingThresh = clip(movingThresh+*thresh_incr,thresh,4*thresh_incr+thresh)
         
out_neurons=NeuronGroup(nb_out_neurons,eqs,threshold='v>movingThresh', reset=rst)
out_neurons.lastSuccessfulThresh=out_neurons.thresh
out_neurons.reset=0
out_neurons.movingThresh=out_neurons.thresh
out_neurons.v=out_neurons.vl
out_neurons.g=0
out_neurons.increment_thresh=0


#(event-driven)
#(event-driven)
synapsesModel='''aPre = 0.25*mV : volt 
                    aPost = 0.5*mV : volt
                    wMax = 10*mV : volt
                    wMin = -10*mV : volt
                    w : volt
                    wp:volt
                    wn:volt
                    tPre : second
                    tPost : second
                    tLTP : second
                    tSTDP = 2 * ms: second
                    condSTDPPre : 1
                    condSTDPPost : 1
                    diffTPost : second
                   '''
 
preEqs = '''tPre = t
        cond=((tPre - lastInhib_post >= tInhib) or (lastInhib_post == 0 * ms)) and ((tPre-lastSpike_post>tRefrac) or (lastSpike_post==0*ms) )
        v_post += w * (cond)
        
        condSTDPPre=(tPost>0*ms) and (tPre > tPost) and (tPre - tPost < tSTDP)
        wn=(clip(w - condSTDPPre* aPre , wMin, wMax) * apprentissage_post)
        w = w * (1 - apprentissage_post) + wn
        '''
#and not_refractory

postEqs = '''tPost = t
         diffTPost=tPost - tPre
         condSTDPPost = ((tPre>0*ms) and (tPost > tPre) and (diffTPost < tSTDP))
         
         wp= (clip(w + condSTDPPost * aPost, wMin, wMax) * apprentissage_post)
         w = w * (1 - apprentissage_post) + wp 
         '''

synapses = Synapses(source=in_neurons, target=out_neurons, model=synapsesModel,
    pre=preEqs, post=postEqs,name="synapses")


#synapses.tSTDP[:]=20*ms
synapses.connect(True)
out_neurons.apprentissage=True;

#for i in range(nb_in_neurons):
#    for  j in range(nb_out_neurons):
#        synapses.w[i,j] = random.random()*out_neurons.gmax

synapses.w[0,0]=synapses.wMax
synapses.w[0,1]=0
synapses.w[1,0]=0
synapses.w[1,1]=synapses.wMax

inSpikes=SpikeMonitor(in_neurons,name='inSpikes')
outSpikes=SpikeMonitor(out_neurons,name='outSpikes')

synState=StateMonitor(synapses,('w','wp','wn','tPre','tPost','condSTDPPre','condSTDPPost','diffTPost','tSTDP'),record = True,name='synState')
outState=StateMonitor(out_neurons,('v','movingThresh','lastSuccessfulThresh','lastSpike','increment_thresh'),record = True,name='outState')

#print outState.ge
visu.save_synapses_as_img(synapses,[0,1], "easy_syns_0",[1,2],vmin=synapses.wMin/volt,vmax=synapses.wMax/volt);

net=Network()
net.add(in_neurons)
net.add(out_neurons)
net.add(synapses)
net.add(synState)
net.add(outState)
net.add(inSpikes)
net.add(outSpikes)


def run_debug(long,idx):
    net.run(long,report="stdout")
    # visu.save_statemonitor_as_plot(outState.v,[0,1], 'easy_v_1')    
    # visu.save_statemonitor_as_plot(outState.movingThresh,[0,1], 'easy_mthresh_1')    
    # visu.save_statemonitor_as_plot(outState.lastSuccessfulThresh,[0,1], 'easy_lastthresh_1')    
    # visu.save_statemonitor_as_plot(outState.lastSpike,[0,1], 'easy_lastSpike_1')  
    # visu.save_statemonitor_as_plot(outState.increment_thresh,[0,1], 'increment_thresh_1') 
      
    visu.save_synapses_as_img(synapses,[0,1], "easy_syns_%d"%idx,size=[2,2],vmin=synapses.wMin/volt,vmax=synapses.wMax/volt);
    visu.display_static_statemonitor_as_string(synapses.tPre,[0,1,2,3], 'easy_tPre_%d'%idx) 
    visu.display_static_statemonitor_as_string(synapses.tPost,[0,1,2,3], 'easy_tPost_%d'%idx) 
    visu.display_static_statemonitor_as_string(synapses.condSTDPPre,[0,1,2,3], 'easy_condSTDPPre_%d'%idx) 
    visu.display_static_statemonitor_as_string(synapses.condSTDPPost,[0,1,2,3], 'easy_condSTDPtPost_%d'%idx) 
    visu.display_static_statemonitor_as_string(synapses.diffTPost,[0,1,2,3], 'easy_diffTPost_%d'%idx) 
    
run_debug(6*ms,1)
run_debug(5*ms,2)
run_debug(5*ms,3)
run_debug(5*ms,4)
run_debug(5*ms,5)