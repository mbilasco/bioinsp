from brian2 import *
import random
from numpy import *

import visu

nb_in_neurons=2
nb_out_neurons=2

in_neurons=SpikeGeneratorGroup(2,indices=array([0,0,0,1,1,1]),times=(array([5,10,15,15,20,25])+2)*ms)

taum = 2*ms
taupre = 4*ms
taupost = taupre
Ee = 0*mV
vt = -54*mV
vr = -60*mV
El = -74*mV
taue = 5*ms
F = 15*Hz
gmax = 10*mV
dApre_i = 1 *mV
dApost_i = -dApre_i * taupre / taupost * 1.05
#dApost_i *= gmax/mV
#dApre_i *= gmax/mV

eqs = '''
        dv/dt = (ge * (Ee-vr) + El - v) / taum : volt
        dge/dt = -ge / taue : 1
        ''' 
out_neurons=NeuronGroup(nb_out_neurons,eqs,threshold='v>vt', reset='v = vr')
#(event-driven)
#(event-driven)
synapsesModel='''w : volt
                dApre/dt = -Apre / taupre : volt 
                dApost/dt = -Apost / taupost : volt
               ''';
#
preEqs=''' ge += w/mV
          Apre += dApre_i
          w = clip(w + Apost, 0*mV, gmax)
          '''
#
postEqs='''Apost += dApost_i
           w = clip(w + Apre, 0*mV, gmax)
           ''',
synapses = Synapses(source=in_neurons, target=out_neurons, model=synapsesModel,
    pre=preEqs, post=postEqs,name="synapses")


#synapses.tLTP=presentation_window*ms
synapses.connect(True)

#for i in range(nb_in_neurons):
#    for  j in range(nb_out_neurons):
#        synapses.w[i,j] = random.random()*out_neurons.gmax

synapses.w[0,0]=gmax;
synapses.w[0,1]=0*mV
synapses.w[1,0]=0*mV
synapses.w[1,1]=gmax;

inSpikes=SpikeMonitor(in_neurons,name='inSpikes')
outSpikes=SpikeMonitor(out_neurons,name='outSpikes')

synState=StateMonitor(synapses,('w','Apre','Apost','ge'),record = True,name='synState')
outState=StateMonitor(out_neurons,('v','ge'),record = True,name='outState')

net=Network()
net.add(in_neurons)
net.add(out_neurons)
net.add(synapses)
net.add(synState)
net.add(outState)
net.add(inSpikes)
net.add(outSpikes)

#print outState.ge

net.run(13*ms,report="stdout")
#print outState.ge
visu.save_statemonitor_as_plot(outState.v,[0,1], 'easy_v_1')    

net.run(26*ms,report="stdout")
#print outState.ge
visu.save_statemonitor_as_plot(outState.v,[0,1], 'easy_v_2')


