from brian2 import *
from numpy import *
from random import gauss
from brian2.input.spikegeneratorgroup import SpikeGeneratorGroup
from models import NeuronMvgThsldVMaxInhibitionRefract_M,SynapsesSTDPRefract_M,SynapsesInhib_M

prefs['codegen.target'] = 'numpy'

def one_layer_archi(inputtrains, nb_out_neurons, presentation_window, wMin=2, wMax=10, wAverage=5, wDeviation=2):

    nb_in_neurons = 28*28
    print nb_in_neurons, " - ", inputtrains[0], " - ", inputtrains[1]
    
    
    in_neurons=SpikeGeneratorGroup(N=28*28,indices=inputtrains[0],times=inputtrains[1]*ms,name="inLayer");
    neuron_model=NeuronMvgThsldVMaxInhibitionRefract_M(presentation_window);

    out_neurons=NeuronGroup(nb_out_neurons,neuron_model.model,threshold=neuron_model.thshld, reset=neuron_model.rst,name='outLayer')
    out_neurons.v=out_neurons.vl
    out_neurons.movingThresh=out_neurons.thresh
    #out_neurons.v=out_neurons.vl
    out_neurons.increment_thresh=1

    synapses_mdl=SynapsesSTDPRefract_M(wMin=-0.15,wMax=0.15,tSTDP=0.75*presentation_window,aPre=0.005,aPost=0.005)
    
    synapses = Synapses(source=in_neurons, target=out_neurons, model=synapses_mdl.model,
        pre=synapses_mdl.preEqs, post=synapses_mdl.postEqs,name="synapses")
    out_neurons.apprentissage=True;
    synapses.connect(True)
    
    inhib_mdl=SynapsesInhib_M();
    inhib = Synapses(source=out_neurons,target=out_neurons,model=inhib_mdl.model,
                     pre=inhib_mdl.pre, name="inhib")

    inhib.connect("i!=j");
    out_neurons.inhibition=True;
    #out_neurons.threshInhibition=presentation_window/2 * ms;
    
    for i in range(nb_in_neurons):
        for  j in range(nb_out_neurons):
            synapses.w[i,j] = (random.random()*(0.15-0)+0)*mV;
            synapses.tPre[i,j] = 0*ms;
            synapses.tPost[i,j] = 0*ms;

    net=Network();
    net.add(in_neurons)
    net.add(out_neurons)
    net.add(synapses)
    net.add(inhib)
    
    return net
