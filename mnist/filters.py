def filter_images_by_label(images,labels,selected_labels,max_samples=0,offset=0):
    result_list=[]
    list_labels=[]
    count = 0; i=offset
    while (i<len(images)-1) and (max_samples==0 or count<max_samples) :
        img=images[i]; label=labels[i]
        if label in selected_labels :
            result_list.append(img)
            list_labels.append(label)
            count+=1
        i+=1
    return list_labels,result_list